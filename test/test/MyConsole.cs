﻿using System;

namespace test
{
    class MyConsole
    {
        public static int Readint()
        {
            try
            {
                return int.Parse(Console.ReadLine());
            }
            catch
            {
                return -1;
            }
            
        }
    }

}


