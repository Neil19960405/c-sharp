﻿using System;
namespace test
{
    class Player
    {
        int power, exp, bag, bakutanNum;

        public void State()
        {
            Console.WriteLine($"力量:{power},經驗值{exp},包包道具數量{bag}");
        }

        public void Fight(Monster[] m1)
        {
            int fightFlag = 0;
            int monsterKey = -1;
            int playerAction = -1;//
            while (fightFlag != 1)
            {
                Console.WriteLine("請選擇您的對手(0~9)");
                monsterKey = MyConsole.Readint();
                if (monsterKey < 0 || monsterKey > 9)
                {
                    Console.WriteLine("請輸入正確的數字");
                    continue;
                }
                Console.WriteLine($"怪物總血量:{m1[monsterKey].GetMaxHp()}\n怪物目前血量:{m1[monsterKey].GetMonsterHp()}\n怪物狀態{m1[monsterKey].GetState()}");
                Console.WriteLine("是否要攻擊此怪? 是(0)否(1)逃跑(2)");
                playerAction = MyConsole.Readint();
                switch (playerAction)
                {
                    case 0:
                        if (m1[monsterKey].GetState() != "死亡")
                        {
                            Console.WriteLine("請使用你的必殺技：\n(3)來跨歐郎甲錐夠 (4)島灰");
                            playerAction = MyConsole.Readint();
                            if (playerAction == 3 || playerAction == 4)
                            {
                                Attack(playerAction, m1[monsterKey]);
                            }
                            else
                            {
                                Console.WriteLine("請輸入正確的指令");
                            }
                        }
                        else
                        {
                            Console.WriteLine("你一定抖愛抹哇欸 (5)馬奇阿米");
                            playerAction = MyConsole.Readint();
                            if (playerAction == 5)
                            {
                                Attack(playerAction, m1[monsterKey]);
                            }
                            else
                            {
                                Console.WriteLine("請輸入正確的指令");
                            }
                        }
                        break;
                    case 1: break;
                    case 2: fightFlag = 1; break;
                    default: Console.WriteLine("請輸入正確的指令"); break;
                }
            }
        }

        void Attack(int playerAction, Monster m1)
        {       
            if (playerAction == 3)
            {
                AttackDetail(50, m1,"錐夠");
            }
            else if (playerAction == 4)
            {   
                if (bakutanNum == 0)
                {
                    Console.WriteLine("島灰不足");
                }
                else
                {
                    AttackDetail(100, m1,"島灰");
                    bakutanNum--;
                }
            }
            else if (playerAction == 5)
            {
                m1.relive();
            }
            else
            {
                Console.WriteLine("請輸入正確的指令");
            }
        }
        void AttackDetail(int damage,Monster m1,string type)
        {
            string monsterStatus;
            monsterStatus = m1.BeAttacked(damage);

            if (monsterStatus == "死亡")
            {
                exp += m1.GetMonsterEXP();
                Console.WriteLine($"怪死掉了，你獲得了{m1.GetMonsterEXP()}點經驗值，你現在共有{exp}點經驗值");
                if (new Random().NextDouble() < 0.5f)
                {
                    bakutanNum++;
                    Console.WriteLine("你拾獲了一枚島灰");
                }
            }
            else
            {
                Console.WriteLine($"你的{type}造成了{damage}點傷害，怪的生命扣至{m1.GetMonsterHp()}");
            }
        }
        private static Player _instance = null;


        private Player(int power, int exp, int bag, int bakutanNum)
        {
            this.power = power;
            this.exp = exp;
            this.bag = bag;
            this.bakutanNum = bakutanNum;
        }
        public static Player Instance
        {
            get 
            {
                if (_instance == null)
                   {
                      _instance = new Player(50,0,0,0);
                   }
                return _instance;
            }
        }
    }

}


