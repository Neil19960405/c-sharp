﻿using System;
using System.Collections.Generic;
using System.Text;

namespace test
{
    class Bag
    {
        private int bagSize;
       
        private Bag(int bagsize)
        {
            this.bagSize = bagsize;
        }
        //singleton
        static private Bag _instance = null;
        public static Bag Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new Bag(20);
                }
                return _instance;
            }
        }
        
        public Dictionary<string,int> BagItem = new Dictionary<string, int>();

       

        public void PutItemsIn(string itemType,int itemNum)
        {
            if (BagItem.Count < bagSize)//判斷包包滿了沒
            {
                int flag = 0;
                if (BagItem.TryGetValue(itemType, out flag) == false)
                {
                    BagItem.Add(itemType, itemNum);
                }
                else
                {
                    BagItem[itemType] = itemNum;
                }
            }
            else//包包滿
            {
                Console.WriteLine("包包已經滿了");
            }
        }
        public void takeItemsOut(string itemType,int num)
        {
            BagItem[itemType] = num;
           if(num == 0)
            {
                BagItem.Remove(itemType);
            }
         
        }
        public void OpenBag()
        {
            foreach (var item in BagItem)
            {
                Console.WriteLine($"{item.Key}:{item.Value}個");
            }
        }
    }
}
