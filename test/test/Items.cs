﻿using System;
using System.Collections.Generic;
using System.Text;

namespace test
{
    class Items
    {
        protected string itemName;
        protected int num=0;
        public void GetItem(double prob)
        {
            if (new Random().NextDouble() < prob)
            {
                num++;
                Bag.Instance.PutItemsIn(itemName, num);
            }
        }
        public virtual void UseItem()
        {}
    }
    
    
    class Bakutan:Items
    {
        public Bakutan()
        {
            itemName = "炸彈";
        }
        public override void UseItem()
        {
            if (num > 0)
            {
                num--;
                Bag.Instance.takeItemsOut(itemName, num);
            }
            else
            {
                Console.WriteLine($"{itemName}不足");
            }
        }
    }
    class Medician:Items
    {
        public Medician()
        {
            itemName = "補血藥";
        }
        public override void UseItem()
        {
            if (num > 0)
            {
                num--;
                Bag.Instance.takeItemsOut(itemName, num);
            }
            else
            {
                Console.WriteLine($"{itemName}不足");
            }
        }
    }
}
