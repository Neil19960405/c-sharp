﻿using System;
namespace test
{
    class Monster
    {

        int hp;//怪物目前血量
        int maxHp;//怪目最大血量
        int exp;//怪物經驗值
        string state;//怪物狀態

        public Monster(int mhp = 200, int mMaxHp = 200, int mExp = 10, string mState = "滿血")
        {
            this.hp = mhp;
            this.maxHp = mMaxHp;
            this.exp = mExp;
            this.state = mState;
        }

        public string BeAttacked (int power)
        {
            hp = hp - power;
            if (hp <= 0)
            {
                hp = 0;
                state = "死亡"; 
            }
            else
            {
               state = "失血";
            }
            return state;
        }
        public void relive()
        {
            hp = 200;
            state = "滿血";
            Console.WriteLine($"因為抹了馬奇阿米，怪的生命加至{hp}，復活了" +
                $"");
        }
        public int GetMonsterEXP()
        {
            return exp;
        }
        public int GetMonsterHp()
        {
            return hp;
        }
        public int GetMaxHp()
        {
            return maxHp;
        }
        public string GetState()
        {
            return state;
        }
    }

}


