﻿using System;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            MyFunction function = new MyFunction();
            Monster[] m1 = MyFunction.CreateMonster(10);
            string flag = "0";
            while (flag != "1")
            {
                Console.WriteLine("請輸入您要的動作：\n 逃跑(0) 顯示狀態(1) 進入戰鬥(2)  ");
                string chooseFight = Console.ReadLine();//玩家輸入
                switch (chooseFight)
                {
                    case "0": flag = function.Run(); break;
                    case "1": Player.Instance.State(); break;
                    case "2": Player.Instance.Fight(m1); break;
                    default: Console.WriteLine("請輸入正確指令"); break;
                }
            }

        }
    }

}


